﻿using RobotController.Grid;
using RobotController.Grid.Tile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace RobotController.Robot
{
    public class SimpleRobot : IControllableRobot
    {
        public SimpleRobot(Simple2DGrid grid, Position robotStartingPosition)
        {
            this.Grid = grid;
            this.StartingPosition = robotStartingPosition;
            this.SetPosition(this.StartingPosition);
        }

        /// <summary>
        /// This event is triggered when the robot's location or rotation is changed.
        /// </summary>
        public event EventHandler<TileEventArgs> PositionChanged;
        public event EventHandler<TileEventArgs> UnableToChangePosition;
        public event EventHandler<TileEventArgs> TileEncountered;

        public Simple2DGrid Grid { get; private set; }

        public Position CurrentPosition { get; private set; }
        public Position PreviousPosition { get; private set; }
        public Position StartingPosition { get; private set; }

        /// <summary>
        /// The robot's current position.
        /// </summary>
        public virtual Position GetPosition()
        {
            return this.CurrentPosition;
        }

        /// <summary>
        /// Move the robot to a new position.
        /// </summary>
        public virtual void SetPosition(Position desired)
        {
            var success = this.Grid.TryMove(this.CurrentPosition, desired, out Tuple<Position, BaseTile> movementInfo);

            if (success)
            {
                this.PreviousPosition = this.CurrentPosition;
                this.CurrentPosition = movementInfo.Item1;

                var tileEventArgs = new TileEventArgs(movementInfo.Item2);

                this?.TileEncountered?.Invoke(this, tileEventArgs);
                this?.PositionChanged?.Invoke(this, tileEventArgs);
            }
            else
            {
                this?.UnableToChangePosition?.Invoke(this, new TileEventArgs(movementInfo.Item2));
            }
        }

        /// <summary>
        /// Rotates the robot without changing its location.
        /// </summary>
        public virtual void Rotate(Rotation rotation)
        {
            CardinalDirection newDirection = Position.RotateCardinalDirection(this.CurrentPosition.Direction, rotation);
            Position newPosition = new Position(this.CurrentPosition.Location, newDirection);

            this.PreviousPosition = this.CurrentPosition;
            this.CurrentPosition = newPosition;
        }

        /// <summary>
        /// Gets a position that is moved 1 unit forward from the robot's current position based on which direction the robot is currently facing.
        /// </summary>
        public virtual Position GetAdvancedPosition()
        {
            Point coordinates = this.CurrentPosition.Location;

            switch ((CardinalDirection)Math.Abs((int)this.CurrentPosition.Direction))
            {
                case CardinalDirection.North:
                    coordinates = new Point(coordinates.X, coordinates.Y + 1);
                    break;

                case CardinalDirection.East:
                    coordinates = new Point(coordinates.X + 1, coordinates.Y);
                    break;

                case CardinalDirection.South:
                    coordinates = new Point(coordinates.X, coordinates.Y - 1);
                    break;

                case CardinalDirection.West:
                    coordinates = new Point(coordinates.X - 1, coordinates.Y);
                    break;
            }

            return new Position(coordinates, this.CurrentPosition.Direction);
        }
    }
}
