﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotController.Command
{
    public class RobotNoOpCommand : RobotCommand
    {
        internal RobotNoOpCommand()
            : base(-1, string.Empty)
        {
        }

        public RobotNoOpCommand(int id, string data)
            : base(id, data)
        {
        }
    }
}
