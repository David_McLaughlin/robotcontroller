﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotController.Grid
{
    /// <summary>
    /// Stores location and rotation
    /// </summary>
    public struct Position
    {
        public Position(int xPosition, int yPosition, CardinalDirection direction) : this(new Point(xPosition, yPosition), direction)
        {
        }
        public Position(Point location, CardinalDirection direction)
        {
            this.Location = location;
            this.Direction = direction;
        }

        public Point Location { get; set; }
        public CardinalDirection Direction { get; set; }

        public int X { get { return this.Location.X; } }
        public int Y { get { return this.Location.Y; } }

        public override string ToString()
        {
            return string.Format("[X={0}, Y={1}, D={2}]", this.X, this.Y, this.Direction);
        }

        /// <summary>
        /// Apply a rotation value to a CardinalDirection.
        /// </summary>
        public static CardinalDirection RotateCardinalDirection(CardinalDirection currentDirection, Rotation rotateDirection)
        {
            int current = (int)currentDirection;
            int next = (int)rotateDirection;
            int result = (next + current);

            if (result >= 360)
            {
                result = (result % 360);
            }
            else if (result < 0)
            {
                result = (360 + result);
            }

            return (CardinalDirection)result;
        }
    }
}
