﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotController.Grid.Generator.Tests
{
    public class PositionTests
    {
        [TestMethod()]
        public void RotateCardinalDirectionFromRotation_Test()
        {
            CardinalDirection newDirection = Position.RotateCardinalDirection(CardinalDirection.North, Rotation.CW90);
            bool success = (newDirection == CardinalDirection.East);

            Assert.IsTrue(success);
        }
    }
}
