﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RobotController.Robot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RobotController.Grid;
using RobotController.Grid.Tile;
using RobotController.Command;
using System.Drawing;

namespace RobotController.Robot.Tests
{
    [TestClass()]
    public class SimpleRobotTests
    {
        private SimpleRobot InitSimpleRobotInstance()
        {
            Simple2DGrid grid = new Simple2DGrid(0, 0, 32, 32);
            Position startingPosition = new Position(1, 1, CardinalDirection.North);

            SimpleRobot robot = new SimpleRobot(grid, startingPosition);

            return robot;
        }

        [TestMethod()]
        public void SetPosition_GoodInput_Test()
        {
            bool success = false;

            SimpleRobot robot = this.InitSimpleRobotInstance();
            robot.PositionChanged += delegate (object sender, TileEventArgs e)
            {
                success = true;
            };

            robot.SetPosition(robot.CurrentPosition);

            Assert.IsTrue(success);
        }

        [TestMethod()]
        public void SetPosition_BadInput_Test()
        {
            bool moved = false;

            SimpleRobot robot = this.InitSimpleRobotInstance();
            robot.PositionChanged += delegate (object sender, TileEventArgs e)
            {
                moved = true;
            };

            robot.SetPosition(new Position(robot.Grid.Bounds.Width + 1, robot.Grid.Bounds.Height + 1, CardinalDirection.North));

            Assert.IsFalse(moved);
        }

        [TestMethod()]
        public void Rotate_Test()
        {
            SimpleRobot robot = this.InitSimpleRobotInstance();
            robot.Rotate(Rotation.CW90);

            bool success = (robot.CurrentPosition.Direction == CardinalDirection.East);

            Assert.IsTrue(success);
        }

        [TestMethod()]
        public void GetAdvancedPosition_Test()
        {
            SimpleRobot robot = this.InitSimpleRobotInstance();
            Position aPosition = robot.GetAdvancedPosition();

            bool hasSameDirection = (robot.CurrentPosition.Direction == aPosition.Direction);
            bool xLocationIsOneOff = (robot.CurrentPosition.X == (aPosition.X - 1) || robot.CurrentPosition.X == (aPosition.X + 1));
            bool yLocationIsOneOff = (robot.CurrentPosition.Y == (aPosition.Y - 1) || robot.CurrentPosition.Y == (aPosition.Y + 1));
            bool isLocationOneOff = (xLocationIsOneOff || yLocationIsOneOff);

            bool success = (hasSameDirection && isLocationOneOff);

            Assert.IsTrue(success);
        }

        [TestMethod()]
        public void AdjustPositionForEmptyTest()
        {
            SimpleRobot robot = this.InitSimpleRobotInstance();

            EmptyTile tile = new EmptyTile(robot.GetPosition().Location);
            robot.Grid.Obstructions.Add(tile);

            Position newPosition = robot.GetAdvancedPosition();
            var success = robot.Grid.TryMove(robot.CurrentPosition, newPosition, out Tuple<Position, BaseTile> movementInformation);

            bool positionExpected = (newPosition.Location == movementInformation.Item1.Location) && (newPosition.Direction == movementInformation.Item1.Direction);

            Assert.IsTrue(success && positionExpected);
        }

        [TestMethod()]
        public void AdjustPositionForRock_Test()
        {
            SimpleRobot robot = this.InitSimpleRobotInstance();

            RockTile tile = new RockTile(new Point(robot.CurrentPosition.X, robot.CurrentPosition.Y + 1));
            robot.Grid.Obstructions.Add(tile);

            Position oldPosition = robot.GetPosition();
            Position newPosition = robot.GetAdvancedPosition();
            var success = robot.Grid.TryMove(robot.CurrentPosition, newPosition, out Tuple<Position, BaseTile> movementInformation);
            bool positionExpected = (movementInformation.Item1.Location == oldPosition.Location) && (movementInformation.Item1.Direction == oldPosition.Direction);

            Assert.IsTrue(success && positionExpected);
        }

        [TestMethod()]
        public void AdjustPositionForHole_Test()
        {
            SimpleRobot robot = this.InitSimpleRobotInstance();

            Point holeStart = new Point(robot.CurrentPosition.X, robot.CurrentPosition.Y + 1);
            Point holeEnd = new Point(holeStart.X + 4, holeStart.Y + 4);

            HoleTile tile = new HoleTile(holeStart, holeEnd);
            robot.Grid.Obstructions.Add(tile);

            Position newPosition = robot.GetAdvancedPosition();
            var success = robot.Grid.TryMove(robot.CurrentPosition, newPosition, out Tuple<Position, BaseTile> movementInformation);
            bool positionExpected = (movementInformation.Item1.Location == holeEnd) && (newPosition.Direction == movementInformation.Item1.Direction);

            Assert.IsTrue(success && positionExpected);
        }

        [TestMethod()]
        public void AdjustPositionForSpinner_Test()
        {
            SimpleRobot robot = this.InitSimpleRobotInstance();
            SpinnerTile tile = new SpinnerTile(robot.CurrentPosition.X, robot.CurrentPosition.Y + 1, Rotation.CW90);
            robot.Grid.Obstructions.Add(tile);

            Position newPosition = robot.GetAdvancedPosition();
            var success = robot.Grid.TryMove(robot.CurrentPosition, newPosition, out Tuple<Position, BaseTile> movementInformation);
            bool directionIsValid = (movementInformation.Item1.Direction == CardinalDirection.East);

            Assert.IsTrue(success && directionIsValid);
        }
    }
}